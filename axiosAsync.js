// could make the axios generic by saying httpClient so can just change inside that same file
// the internal function from below worked with console, so not needed here 
module.exports = async function (uri) {
    const axios = require("axios");
    const response = await axios.get(uri);
    return response.data
}


// // export ansyc function
// module.exports = async function (uri) {
//     // get axios library
//     const axios = require("axios");
//     // const logSpacing = "\n\n\n";
    
//     // const axiosWithAsyncAWait = async uri => {
//     //     // wait get request at that uri; is basically .then; 
//     //     const response = await axios.get(uri);
//     //     const hydratedBody = response.data;
//     //     // // not really needed
//     //     // console.log(logSpacing);
//     //     // console.log("AXIOS library way of making a GET request (using async/await)");
//     //     // console.log("Status code:", response.status);
//     //     // console.log(hydratedBody);
//     //     return hydratedBody;
//     // };
//       // w/e this returns will be in promise; could do .then to get it
//     return axiosWithAsyncAWait(uri);

// }