const axiosClientRequest = require("./axiosAsync.js"); // this is just the http client, so could have called it that; reaching out to server by getting access onto this file; liek function declaration but getting from another file 
const fs = require("fs"); // file system you need to reach to in direactory; access to native libaray to access/interact with file system, like writefilesync, etc; module that is object with bunch of stuff on it 
const url = "http://jservice.io/api/category?id="; // identify url we can to access; could cange to localhost if want 
const categoriesIdArray = [67, 780, 277, 223, 184, 680, 21, 309, 582, 267, 136, 249, 105, 770, 508, 561, 420, 37, 1195, 25, 897]; 

const categoryPromises = categoriesIdArray.map(id => axiosClientRequest(url + id)) // map goes through array and trasform original array ijto new array; first is just numbers but then each item gets composition of the url and the id from the axiosclientcall
Promise.all(categoryPromises) // wait for stuff to resolve
    .then(categories => // array of category promises; array of categories 
        fs.writeFileSync("./categories.json", JSON.stringify(categories)) // make categories.json file or overwrite it; the .json 
        // for serving json against server, might do the reading and respond with stuff back to user
    )


// // what funciton is:
// function map (array, callback) {
//     const newArray = [];
//     // for each item inside, pass in given item into original array then makes reutrn array based on callback
//     for (let i = 0; i < array.length; i++) {
//         // newArray[i] = callback(i)
//         newArray.push(callback(array[i]))
//     }
//     return newArray
// }


// // this sis async; could wrap in async in the nativeWay.js; bbring in module 
// axiosClientRequest("http://jservice.io/api/category?id=" + categoriesIdArray)
//     // just having it like this would overwrite file by doing multiple client requests and combine them into obj like promise.all 
//     .then(category => fs.writeFileSync("./categories.json", JSON.stringify(category))) // category is promised obj/response data; the category means the response body, soc ould be w/e; know that the body is a category, so name it that; the .json is hydrated as json
// // might need slash for root?; this immediately treies to write with Sync, 
// // fs.writeFileSync("./categories.json", category);